# Java Assignment 3 - API
This project was created as part of an assigment in Java. This application allows for the users to use this API to get information about movies, characters and franchises.

## Installation

### Downloading
The application is free to clone straight from gitlab. Type this into your selected git console to get the current main version:
```
git clone git@gitlab.com:AdrianMattsson/java-assignment-3.git
```
Git will copy the repository onto your machine.

## Usage
### Running the application
The application is very simple to run, just compile it in your IDE of choice and run HibernateMovieApiApplication.

In your application.properties, paste this code below and switch the placeholders to your database url, username and password.
```
spring.datasource.url= <PLACEHOLDER>
spring.datasource.username= <PLACEHOLDER>
spring.datasource.password= <PLACEHOLDER>
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.hibernate.ddl-auto=create
logging.level.org.hibernate.state=trace
spring.jpa.show-sql=true
spring.jpa.defer-datasource-initialization=true
spring.sql.init.mode=always
spring.jackson.serialization.indent_output = true
springdoc.swagger-ui.operationsSorter=method
```

### Functionality
The application supports full CRUD functionality for characters, movies and franchises.
Use OpenAPI to get further documentation about the application and its API.


## Contributors
Adrian Mattsson @AdrianMattsson

Huwaida Al Hamdawee @Huwaida-al
