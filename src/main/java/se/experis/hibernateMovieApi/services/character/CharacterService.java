package se.experis.hibernateMovieApi.services.character;

import se.experis.hibernateMovieApi.models.Character;
import se.experis.hibernateMovieApi.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface CharacterService extends CrudService<Character, Integer> {
}
