package se.experis.hibernateMovieApi.services.movie;

import se.experis.hibernateMovieApi.exceptions.MovieNotFoundException;
import se.experis.hibernateMovieApi.models.Movie;
import se.experis.hibernateMovieApi.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService{
    private MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findByID(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        movieRepository.deleteById(integer);
    }
}
