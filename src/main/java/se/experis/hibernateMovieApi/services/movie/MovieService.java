package se.experis.hibernateMovieApi.services.movie;

import se.experis.hibernateMovieApi.models.Movie;
import se.experis.hibernateMovieApi.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface MovieService extends CrudService<Movie, Integer> {
}
