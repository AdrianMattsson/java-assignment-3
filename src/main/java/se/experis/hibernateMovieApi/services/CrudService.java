package se.experis.hibernateMovieApi.services;

import java.util.Collection;

public interface CrudService <T, ID>{
    T findByID(ID id);
    Collection<T> findAll();
    T add(T entity);
    T update(T entity);
    void deleteById(ID id);
}
