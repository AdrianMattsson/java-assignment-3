package se.experis.hibernateMovieApi.services.franchise;

import se.experis.hibernateMovieApi.exceptions.FranchiseNotFoundException;
import se.experis.hibernateMovieApi.models.Franchise;
import se.experis.hibernateMovieApi.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    private FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findByID(Integer id) {
        return franchiseRepository.findById(id)
                .orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        franchiseRepository.deleteById(integer);
    }
}
