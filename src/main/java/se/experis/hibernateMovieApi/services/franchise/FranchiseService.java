package se.experis.hibernateMovieApi.services.franchise;

import se.experis.hibernateMovieApi.models.Franchise;
import se.experis.hibernateMovieApi.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface FranchiseService extends CrudService<Franchise, Integer> {
}
