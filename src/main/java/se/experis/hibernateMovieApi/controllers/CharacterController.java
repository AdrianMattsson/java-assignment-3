package se.experis.hibernateMovieApi.controllers;

import se.experis.hibernateMovieApi.mapper.CharacterMapper;
import se.experis.hibernateMovieApi.models.Character;
import se.experis.hibernateMovieApi.models.dto.CharacterDTO;
import se.experis.hibernateMovieApi.services.character.CharacterService;
import se.experis.hibernateMovieApi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "No characters exist",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(characterService.findAll());
    }

    @Operation(summary = "Get character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character with given ID does not exist",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        return ResponseEntity.ok(characterService.findByID(id));
    }

    @Operation(summary = "Add character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Character character) {
        var addedCharacter = characterService.add(character);
        URI uri = URI.create("character/" + addedCharacter.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Update character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character with given ID does not exist",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<Character> update(@RequestBody CharacterDTO characterDTO) {
        characterService.update(characterMapper.characterDTOToCharacter(characterDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character with given ID does not exist",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
