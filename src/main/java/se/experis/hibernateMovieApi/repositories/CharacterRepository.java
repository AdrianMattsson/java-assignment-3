package se.experis.hibernateMovieApi.repositories;

import se.experis.hibernateMovieApi.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character,Integer> {
}
