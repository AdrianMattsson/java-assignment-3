package se.experis.hibernateMovieApi.models.dto;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String name;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;
    private Integer franchise;
    private Set<Integer> characters;
}
