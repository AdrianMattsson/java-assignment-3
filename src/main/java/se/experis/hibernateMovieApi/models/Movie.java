package se.experis.hibernateMovieApi.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;

    @Column(name = "movie_title", length = 50, nullable = false)
    private String name;

    @Column(name = "movie_genre", length = 50)
    private String genre;

    @Column(name = "movie_release_year", length = 30)
    private String releaseYear;

    @Column(name = "movie_director", length = 50)
    private String director;

    @Column(name = "movie_picture", length = 200)
    private String picture; //This is meant to be a URL to the source picture

    @Column(name = "movie_trailer", length = 200)
    private String trailer; //This is meant to be a URL to the source video

    @ManyToMany(mappedBy = "movies")
    private Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
