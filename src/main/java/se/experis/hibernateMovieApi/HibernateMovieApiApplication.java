package se.experis.hibernateMovieApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateMovieApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateMovieApiApplication.class, args);
    }

}
