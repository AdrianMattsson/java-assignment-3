package se.experis.hibernateMovieApi.mapper;

import se.experis.hibernateMovieApi.models.Character;
import se.experis.hibernateMovieApi.models.Franchise;
import se.experis.hibernateMovieApi.models.Movie;
import se.experis.hibernateMovieApi.models.dto.MovieDTO;
import se.experis.hibernateMovieApi.services.character.CharacterService;
import se.experis.hibernateMovieApi.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    private CharacterService characterService;
    @Autowired
    private FranchiseService franchiseService;

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapFromFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapFromCharacters")
    public abstract MovieDTO movieToMovieDTO(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapToFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapToCharacters")
    public abstract Movie movieDTOToMovie(MovieDTO movieDTO);

    @Named("mapToCharacters")
    Set<Character> mapToCharacters(Set<Integer> characterIds){
        if (characterIds == null) return null;
        return characterIds.stream().map(i -> characterService.findByID(i)).collect(Collectors.toSet());
    }

    @Named("mapFromCharacters")
    Set<Integer> mapFromCharacters(Set<Character> characters){
        if (characters ==  null) return null;
        return characters.stream().map(Character::getId).collect(Collectors.toSet());
    }

    @Named("mapFromFranchise")
    Integer mapFromFranchise(Franchise franchise){
        if (franchise == null) return null;
        return franchise.getId();
    }

    @Named("mapToFranchise")
    Franchise mapToFranchise(Integer id){
        if(id == null) return null;
        return franchiseService.findByID(id);
    }
}

