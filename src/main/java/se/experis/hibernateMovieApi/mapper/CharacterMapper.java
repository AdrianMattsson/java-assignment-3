package se.experis.hibernateMovieApi.mapper;

import se.experis.hibernateMovieApi.models.Movie;
import se.experis.hibernateMovieApi.models.dto.CharacterDTO;
import se.experis.hibernateMovieApi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import se.experis.hibernateMovieApi.models.Character;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    private MovieService movieService;

    @Mapping(target="movies", source="movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO charToCharDTO(Character character);

    @Mapping(target="movies", source="movies")
    public abstract Collection<CharacterDTO> charToCharDTO(Collection<Character> characters);
    @Mapping(target="movies", source="movies", qualifiedByName = "mapMovieToCharacters")
    public abstract Character characterDTOToCharacter(CharacterDTO characterDTO);

    @Named("mapMovieToCharacters")
    Set<Movie>mapToMovieCharacters(Set<Integer> movieIds){
        return movieIds.stream()
                .map(i -> movieService.findByID(i))
                .collect(Collectors.toSet());
    }

    @Named("moviesToIds")
    Set<Integer> map(Set<Movie> movies){
        if (movies == null) return null;
        return movies.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}


