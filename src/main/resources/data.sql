INSERT INTO movie ("movie_title", "movie_genre") VALUES ('King kong', 'Action-Adventure');
INSERT INTO movie ("movie_title", "movie_genre") VALUES ('Bad Boys', 'Comedy');
INSERT INTO movie ("movie_title") VALUES ('Titanic');
INSERT INTO movie ("movie_title") VALUES ('The Fellowship of the Ring');
INSERT INTO movie ("movie_title", "movie_genre") VALUES ('The Two Towers', 'Action-Adventure');
INSERT INTO movie ("movie_title") VALUES ('The Rise of Skywalker');
INSERT INTO movie ("movie_title", "movie_release_year") VALUES ('The Last Jedi', '2017');
INSERT INTO movie ("movie_title") VALUES ('Revenge of the Sith');
INSERT INTO movie ("movie_title", "movie_genre") VALUES ('Endgame', 'Action');

INSERT INTO character ("character_name") VALUES ('Andy Serkis');
INSERT INTO character ("character_name") VALUES ('Martin Laurence');
INSERT INTO character ("character_name") VALUES ('Kate Winslet');
INSERT INTO character ("character_name") VALUES ('Bilbo Baggins');
INSERT INTO character ("character_name") VALUES ('Frodo Baggins');
INSERT INTO character ("character_name") VALUES ('Yoda');
INSERT INTO character ("character_name") VALUES ('Leia');
INSERT INTO character ("character_name") VALUES ('Bruce Banner');
INSERT INTO character ("character_name") VALUES ('Tony Stark');

INSERT INTO franchise ("franchise_name") VALUES ('Star Wars');
INSERT INTO franchise ("franchise_name") VALUES ('Avengers');
INSERT INTO franchise ("franchise_name") VALUES ('Lord of the Rings');

INSERT INTO character_movie ("character_id", "movie_id") VALUES (4,4);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (5,4);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (4,5);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (5,5);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (8,9);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (9,9);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (6,8);
INSERT INTO character_movie ("character_id", "movie_id") VALUES (7,8);

UPDATE movie SET franchise_id = 1 WHERE movie_id = 1;